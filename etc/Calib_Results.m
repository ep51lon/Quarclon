% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly excecuted under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 1600 ; 1600 ];

%-- Principal point:
cc = [ 640 ; 360 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.0 ; 0.0 ; -0.0 ; -0.0 ; 0.0 ];

%-- Focal length uncertainty:
fc_error = [ 6.199456784409434 ; 6.716188223221840 ];

%-- Principal point uncertainty:
cc_error = [ 18.283116535352171 ; 14.336549176030054 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.023297423699116 ; 0.066278876186709 ; 0.002127465108546 ; 0.001704147697935 ; 0.000000000000000 ];

%-- Image size:
nx = 1280;
ny = 720;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 17;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -5.639418e-02 ; -1.592278e+00 ; 2.472064e-01 ];
Tc_1  = [ 1.445677e+02 ; -4.624353e+01 ; 2.750760e+02 ];
omc_error_1 = [ 1.002002e-02 ; 1.759156e-02 ; 1.335612e-02 ];
Tc_error_1  = [ 5.569679e+00 ; 4.250566e+00 ; 3.268677e+00 ];

%-- Image #2:
omc_2 = [ -1.839992e-01 ; -2.448287e+00 ; -1.417133e+00 ];
Tc_2  = [ 1.012833e+02 ; -5.795374e+01 ; 2.646892e+02 ];
omc_error_2 = [ 1.197874e-02 ; 1.865709e-02 ; 1.943714e-02 ];
Tc_error_2  = [ 4.934537e+00 ; 4.000791e+00 ; 2.953247e+00 ];

%-- Image #3:
omc_3 = [ -1.239804e+00 ; -1.701691e+00 ; -9.849561e-01 ];
Tc_3  = [ -7.046764e+01 ; -5.603129e+01 ; 2.552836e+02 ];
omc_error_3 = [ 1.061350e-02 ; 1.732289e-02 ; 1.838641e-02 ];
Tc_error_3  = [ 4.708556e+00 ; 3.744200e+00 ; 2.846870e+00 ];

%-- Image #4:
omc_4 = [ -1.512890e-01 ; 2.320477e+00 ; 2.868935e-01 ];
Tc_4  = [ 7.573405e+01 ; -3.882149e+01 ; 4.063429e+02 ];
omc_error_4 = [ 7.816751e-03 ; 1.901858e-02 ; 1.824491e-02 ];
Tc_error_4  = [ 7.412479e+00 ; 5.811932e+00 ; 2.524043e+00 ];

%-- Image #5:
omc_5 = [ -5.891486e-01 ; -2.548399e+00 ; 1.292843e+00 ];
Tc_5  = [ 9.106812e+01 ; -7.695593e+01 ; 3.966848e+02 ];
omc_error_5 = [ 1.461919e-02 ; 1.621678e-02 ; 2.040523e-02 ];
Tc_error_5  = [ 7.265691e+00 ; 5.761143e+00 ; 2.715146e+00 ];

%-- Image #6:
omc_6 = [ -1.303701e+00 ; -2.582823e+00 ; 7.321891e-01 ];
Tc_6  = [ 5.323880e+01 ; -9.699683e+01 ; 4.845394e+02 ];
omc_error_6 = [ 1.320432e-02 ; 1.671913e-02 ; 2.647434e-02 ];
Tc_error_6  = [ 8.886619e+00 ; 6.958268e+00 ; 3.406903e+00 ];

%-- Image #7:
omc_7 = [ 2.744981e-02 ; -2.829484e+00 ; 3.023792e-01 ];
Tc_7  = [ 9.071112e+01 ; -4.863904e+01 ; 2.960178e+02 ];
omc_error_7 = [ 4.623960e-03 ; 1.850680e-02 ; 2.316872e-02 ];
Tc_error_7  = [ 5.503610e+00 ; 4.299668e+00 ; 2.710642e+00 ];

%-- Image #8:
omc_8 = [ -1.985782e-02 ; -1.794576e+00 ; 3.888829e-01 ];
Tc_8  = [ 1.156710e+02 ; -6.184618e+01 ; 3.328615e+02 ];
omc_error_8 = [ 1.007349e-02 ; 1.776171e-02 ; 1.439289e-02 ];
Tc_error_8  = [ 6.345881e+00 ; 4.917872e+00 ; 3.291878e+00 ];

%-- Image #9:
omc_9 = [ -3.364232e-01 ; -1.965874e+00 ; 4.597020e-01 ];
Tc_9  = [ 7.406086e+01 ; -8.121425e+01 ; 3.656501e+02 ];
omc_error_9 = [ 1.026528e-02 ; 1.788112e-02 ; 1.552440e-02 ];
Tc_error_9  = [ 6.779935e+00 ; 5.293918e+00 ; 3.265873e+00 ];

%-- Image #10:
omc_10 = [ -1.777670e+00 ; -1.673958e+00 ; 1.268082e-02 ];
Tc_10  = [ -4.133254e+01 ; -5.671858e+01 ; 3.653479e+02 ];
omc_error_10 = [ 1.190326e-02 ; 1.545154e-02 ; 2.125000e-02 ];
Tc_error_10  = [ 6.652283e+00 ; 5.223301e+00 ; 2.893723e+00 ];

%-- Image #11:
omc_11 = [ -1.665214e+00 ; -1.485990e+00 ; -4.797456e-01 ];
Tc_11  = [ -6.964293e+01 ; -3.700042e+01 ; 2.757763e+02 ];
omc_error_11 = [ 1.114786e-02 ; 1.596978e-02 ; 1.953819e-02 ];
Tc_error_11  = [ 5.043554e+00 ; 3.963207e+00 ; 2.621093e+00 ];

%-- Image #12:
omc_12 = [ -1.508416e+00 ; -1.282418e+00 ; -6.636966e-01 ];
Tc_12  = [ -6.909748e+01 ; -5.429525e+01 ; 2.693577e+02 ];
omc_error_12 = [ 1.168899e-02 ; 1.574412e-02 ; 1.769895e-02 ];
Tc_error_12  = [ 4.967139e+00 ; 3.899584e+00 ; 2.682315e+00 ];

%-- Image #13:
omc_13 = [ -1.472568e+00 ; -1.093877e+00 ; -7.444434e-01 ];
Tc_13  = [ -5.193001e+01 ; -5.190207e+01 ; 2.996282e+02 ];
omc_error_13 = [ 1.218367e-02 ; 1.577884e-02 ; 1.705784e-02 ];
Tc_error_13  = [ 5.525328e+00 ; 4.307263e+00 ; 2.761259e+00 ];

%-- Image #14:
omc_14 = [ 1.646299e+00 ; 1.736576e+00 ; 9.750336e-01 ];
Tc_14  = [ -7.122641e+01 ; -4.966380e+01 ; 3.052803e+02 ];
omc_error_14 = [ 1.705103e-02 ; 1.256475e-02 ; 2.067515e-02 ];
Tc_error_14  = [ 5.615242e+00 ; 4.441037e+00 ; 3.364823e+00 ];

%-- Image #15:
omc_15 = [ 1.773779e+00 ; 1.147470e-01 ; -1.435942e-01 ];
Tc_15  = [ -7.337158e+01 ; 7.283274e+01 ; 2.187998e+02 ];
omc_error_15 = [ 1.428191e-02 ; 1.215542e-02 ; 1.687871e-02 ];
Tc_error_15  = [ 4.152006e+00 ; 3.255839e+00 ; 2.650136e+00 ];

%-- Image #16:
omc_16 = [ 7.742730e-03 ; -1.914266e+00 ; 1.103889e-01 ];
Tc_16  = [ 1.295670e+02 ; -4.206638e+01 ; 2.983223e+02 ];
omc_error_16 = [ 8.831746e-03 ; 1.734130e-02 ; 1.519342e-02 ];
Tc_error_16  = [ 5.843017e+00 ; 4.505444e+00 ; 3.237959e+00 ];

%-- Image #17:
omc_17 = [ 8.859326e-01 ; -2.538767e+00 ; -5.641368e-01 ];
Tc_17  = [ 1.308052e+02 ; 5.092453e+01 ; 3.522342e+02 ];
omc_error_17 = [ 1.213431e-02 ; 1.754953e-02 ; 2.273129e-02 ];
Tc_error_17  = [ 6.630369e+00 ; 5.172149e+00 ; 3.892597e+00 ];

